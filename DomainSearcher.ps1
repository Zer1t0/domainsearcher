<#
	  Author: Zer1t0
    License: GNU GPLv3
#>

function New-DomainContext{
	[CmdletBinding()]
	param(
        [Parameter(Mandatory=$true)]
		[string]$Domain,
		[string]$User,
		[string]$Password
	)
	if($User){
		if($Password){
			Write-Verbose "DirectoryContext('Domain', $Domain, $User, $Password)"
			return New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext("Domain", $Domain, $User, $Password)
		}
		else{
			Write-Verbose "DirectoryContext('Domain', $Domain, $User)"
			return New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext("Domain", $Domain, $User)
		}
	}
    Write-Verbose "Get domain name of $Domain"
	return New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext("Domain", $Domain)
}

function New-Domain{
	[CmdletBinding()]
	param(
		[string]$Domain,
		[string]$User,
		[string]$Password
	)

	if($Domain){
        $domainContext = New-DomainContext -Domain $Domain -User $User -Password $Password
        return [System.DirectoryServices.ActiveDirectory.Domain]::GetDomain($domainContext).GetDirectoryEntry()
	}
	return [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
}

function New-DirectoryEntry{
	[CmdletBinding()]
	param(
		[string]$Domain,
		[string]$User,
		[string]$Password,
		[ValidatePattern("^((LDAP://)?(CN=.+,)*(OU=.+,)*(DC=.+,)*DC=.+)?`$")]
		[string]$SearchRoot
	)

	if($SearchRoot){
		if(!$SearchRoot.StartsWith("LDAP://")){
			$SearchRoot = "LDAP://$SearchRoot"
		}

		if($User){
			if($Password){
				Write-Verbose "DirectoryEntry($SearchRoot, $User, $Password)"
				return New-Object System.DirectoryServices.DirectoryEntry($SearchRoot, $User, $Password)
			}
			else{
				Write-Verbose "DirectoryEntry($SearchRoot, $User)"
				return New-Object System.DirectoryServices.DirectoryEntry($SearchRoot, $User)
			}
		}
		else{
			Write-Verbose "DirectoryEntry($SearchRoot)"
			return New-Object System.DirectoryServices.DirectoryEntry($SearchRoot)
		}
	}



	$dom = New-Domain -Domain $Domain -User $User -Password $Password
	return $dom.GetDirectoryEntry()
}


function New-ADSearcher{
	[CmdletBinding()]
	param(
		[Parameter(Mandatory=$true)]
		[System.DirectoryServices.DirectoryEntry]$DirectoryEntry,
		[string]$Filter="",
		[string[]]$Fields=@(),
		[uint32]$PageSize=200,
		[ValidateSet("Base","OneLevel","Subtree")]
		[string]$SearchScope = "Subtree",
		[string]$AttributeScopeQuery,
		[ValidatePattern("^((LDAP://)?(CN=.+,)*(OU=.+,)*(DC=.+,)*DC=.+)?`$")]
		[string]$SearchRoot
	)

	$searcher = New-Object System.DirectoryServices.DirectorySearcher($DirectoryEntry)
	if($SearchRoot){
		if(!$SearchRoot.StartsWith("LDAP://")){
			$SearchRoot = "LDAP://$SearchRoot"
		}
		$searcher.$SearchRoot = $SearchRoot
	}

	$searcher.CacheResults = $false
	$searcher.SearchScope = $SearchScope
	$searcher.PageSize = $PageSize
	$searcher.Filter = $Filter
	if($AttributeScopeQuery){
		$searcher.AttributeScopeQuery = $AttributeScopeQuery
	}
	#$searcher.ServerTimeLimit
	#$searcher.SizeLimit

	if($Fields){
		$searcher.PropertiesToLoad.AddRange($Fields)
	}
	$searcher
}


function Build-LDAPFilter {
	param(
		[switch]$Computer,
		[switch]$Group,
		[switch]$User,
		[switch]$OU,
		[switch]$Admin,
		[string]$SamAccountName,
		[string]$Name,
		[string]$DNSHostName,
		[string]$SID,
		[string]$MemberOf,
		[string]$Member,
		[string]$Filter=""
	)

	$TypesFilter = ""

	if($Computer){ $TypesFilter = "(|(samAccountType=805306369)$TypesFilter)"}
	if($Group){ $TypesFilter = "(|(objectCategory=group)$TypesFilter)"}#samAccountType=268435456
	if($User){ $TypesFilter = "(|(samAccountType=805306368)$TypesFilter)" } #0x30000000
	if($OU) {  $TypesFilter = "(|(objectCategory=organizationalUnit)$TypesFilter)" }
	if($TypesFilter){ $Filter = "(&($TypesFilter)$Filter)"}

	if($DNSHostName){ $Filter = "(&(dnshostname=$DNSHostName)$Filter)"}
	if($Name){ $Filter = "(&(name=$Name)$filter)"}
	if($SamAccountName){ $Filter = "(&(samaccountname=$SamAccountName)$Filter)"}
	if($SID){ $Filter = "(&(objectsid=$SID)$Filter)"}
	if($Admin){ $Filter = "(&(admincount=1)$Filter)"}

	if($MemberOf){ $Filter = "(&(memberof=$MemberOf)$Filter)" }
	if($Member){ $Filter = "(&(member=$Member)$Filter)" }

	return $Filter
}

function Find-ADObjects{
	<#
	    .SYNOPSIS
	    	Function to retrieve differents objects of Active Directory domain

	    .DESCRIPTION
	    	Function with some filters to retrieve differents objects... etc

	    .PARAMETER Computer
	    	Add filter to retrieve computer objects

	    .PARAMETER Group
	    	Add filter to retrieve group objects

	    .PARAMETER User
	    	Add filter to retrieve user objects

	    .PARAMETER OU
	    	Add filter to retrieve OU objects

	    .PARAMETER Domain
	    	Add filter to retrieve domain objects

	    .PARAMETER Admin
	    	Add filter to retrieve objects with admincount equals to 1

	    .PARAMETER One
	    	Only retrieves one object

	    .PARAMETER SamAccountName
	    	Allows retrieve only object with an specified samAccountName value

	    .PARAMETER Name
	    	Allows retrieve only object with an specified name value

	    .PARAMETER DNSHostName
	    	Allows retrieve only object with an specified DNSHostName value

	    .PARAMETER SID
	    	Allows retrieve only object with an specified SID value

	    .PARAMETER Member
	    	Allows retrieve objects which contains as member the object specified

	    .PARAMETER MemberOf
	    	Allows retrieve objects which are members of object specified objects "MemberOf"

	    .PARAMETER Filter
	    	Allows specify a custom LDAP filter

	    .PARAMETER Fields
	    	Allows specify which properties of object are retrieved
	    	Note: adspath is always retrieved

	    .PARAMETER SearchRoot
	    	Allows specify the LDAP path which will be the root in search

	    .PARAMETER SearchScope
	    	Allows specify the scope of the search. Possible values are:
	    		Base: Only search in root base object
	    		OneLevel: Search in inmediate children of base object, but excludes itself
	    		Subtree: Search in all childs of base object, included itself (Default)

	    .EXAMPLE

			# Retrieve admin users
	    	Get-DomainObject -User -Admin

	    	# Retrieve names of members of group Administrators
	    	Get-DomainObject -MemberOf Administrators -Fields name

	#>
	[CmdletBinding()]
	param(
		[switch]$Computer,
		[switch]$Group,
		[switch]$User,
		[switch]$OU,
		[string]$Domain,
		[System.DirectoryServices.ActiveDirectory.DirectoryContext]$DomainContext,
		[switch]$Admin,
		[switch]$One,
		[string]$SamAccountName,
		[string]$Name,
		[string]$DNSHostName,
		[string]$SID,
		[string]$Member,
		[string]$MemberOf,
		[string]$Filter="",
		[string[]]$Fields=@(),
		[ValidatePattern("^((LDAP://)?(CN=.+,)*(OU=.+,)*(DC=.+,)*DC=.+)?`$")]
		[string]$SearchRoot,
		[ValidateSet("Base","OneLevel","Subtree")]
		[string]$SearchScope = "Subtree",
		[string]$CredUser,
		[string]$CredPassword
	)

	if($Verbose){
		$VerboseBackup = $VerbosePreference
		$VerbosePreference = "Continue"
	}

	$Filter = Build-LDAPFilter -Filter:$Filter -Computer:$Computer -Group:$Group -User:$User -OU:$OU -Admin:$Admin -SamAccountName:$SamAccountName -Name:$Name -DNSHostName:$DNSHostName -SID:$SID -MemberOf:$MemberOf


	if(!$DomainContext){
		$DirectoryEntry = New-DirectoryEntry -Domain:$Domain -User:$User -Password:$Password -SearchRoot:$SearchRoot
	}else{
		$DirectoryEntry = [System.DirectoryServices.ActiveDirectory.Domain]::GetDomain($DomainContext).GetDirectoryEntry()
	}

	$searcher =  New-ADSearcher -DirectoryEntry $DirectoryEntry  -Filter $Filter -Fields $Fields -SearchScope $SearchScope -AttributeScopeQuery $AttributeScopeQuery

	$(if($One){
		$searcher.findOne()
	}else{
		$searcher.findAll()
	}) | % { $_.properties }

	if($Verbose){
		$VerbosePreference = $VerboseBackup
	}
}
